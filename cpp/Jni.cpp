
#include "Jni.hpp"

#include <array>

#include "Debug.hpp"




namespace Flat {
namespace Jni {




namespace {

JavaVM * javaVm = nullptr;

JNIEnv * getEnv()
{
	union {
		JNIEnv * nativeEnvironment;
		void * venv;
	} uenv;

	uenv.venv = nullptr;

	const jint ret = javaVm->GetEnv(&uenv.venv, JNI_VERSION_1_4);
	FLAT_UNUSED_CHECK_ERROR(ret == JNI_OK) << "GetEnv failed";

	return uenv.nativeEnvironment;
}

}




void load(JavaVM * const vm, const jint version)
{
	static bool initialized = false;
	if (initialized) return;
	initialized = true;

	javaVm = vm;

	union {
		JNIEnv * nativeEnvironment;
		void * venv;
	} uenv;

	uenv.venv = nullptr;

	if (vm->GetEnv(&uenv.venv, version) != JNI_OK) {
		FLAT_ERROR << "GetEnv failed";
		throw Error("GetEnv failed");
	}
}


jobject localRef(const jobject object) noexcept
{
	const Env env;
	const jobject o = env->NewLocalRef(object);
	FLAT_ASSERT(o);
	return o;
}


void localUnref(const jobject object) noexcept
{
	const Env env;
	env->DeleteLocalRef(object);
}


jobject globalRef(const jobject object) noexcept
{
	const Env env;
	const jobject o = env->NewGlobalRef(object);
	FLAT_ASSERT(o);
	return o;
}


void globalUnref(const jobject object) noexcept
{
	const Env env;
	env->DeleteGlobalRef(object);
}




Error Error::f(char const * format, ...)
{
	va_list args;
	va_start(args, format);
	char buffer[512];
	const int bytes = vsnprintf(buffer, sizeof(buffer), format, args);
	va_end(args);

	FLAT_UNUSED_ASSERT(bytes >= 0);
	return Error(buffer);
}




Env::Env() noexcept :
	env_(getEnv())
{
}


jclass Env::getClass(const char * const name) const
{
	const jclass c = env_->FindClass(name);
	FLAT_CHECK(c) << "Failed to find class:" << name;
	return c;
}


jmethodID Env::getMethod(const jclass c, const char * const name,
		const char * const signature) const
{
	const jmethodID method = env_->GetMethodID(c, name, signature);
	FLAT_CHECK(method) << "Failed to find method:" << name << signature;
	return method;
}


jmethodID Env::getStaticMethod(const jclass c, const char * const name,
		const char * const signature) const
{
	const jmethodID method = env_->GetStaticMethodID(c, name, signature);
	FLAT_CHECK(method) << "Failed to find method:" << name << signature;
	return method;
}


jfieldID Env::getField(const jclass c, const char * const name, const char * const signature) const
{
	const jfieldID field= env_->GetFieldID(c, name, signature);
	FLAT_CHECK(field) << "Failed to find field:" << name << signature;
	return field;
}


jfieldID Env::getStaticField(const jclass c, const char * const name,
		const char * const signature) const
{
	const jfieldID field= env_->GetStaticFieldID(c, name, signature);
	FLAT_CHECK(field) << "Failed to find static field:" << name << signature;
	return field;
}


void Env::registerNatives(const jclass c, const Method * const methods, const int count) const
{
	FLAT_UNUSED(c)
	FLAT_UNUSED(methods)
	FLAT_UNUSED(count)

	FLAT_CHECK(env_->RegisterNatives(c, reinterpret_cast<const JNINativeMethod*>(methods),
			count) == JNI_OK) << "Failed to register natives";
}


LocalString Env::newString(const char * const data) const
{
	return LocalString::adopt(env_->NewStringUTF(data));
}


Env::String::String(String && other) noexcept :
	env(other.env),
	string(other.string),
	data(other.data)
{
	other.string = nullptr;
}


void Env::String::operator=(String && other) noexcept
{
	std::swap(string, other.string);
	std::swap(data, other.data);
}


Env::String::~String() noexcept
{
	if (string) {
		env->ReleaseStringUTFChars(string, data.data());
	}
}


Env::String Env::lockString(const jstring string) const
{
	const int length = env_->GetStringUTFLength(string);
	const char * const data = env_->GetStringUTFChars(string, nullptr);
	FLAT_CHECK(data);
	return String(*this, string, std::string_view(data, length));
}


std::string Env::toStdString(const jstring string) const
{
	const auto s = this->lockString(string);
	return std::string(s.data);
}




Lock::Private Lock::_private() noexcept
{
	union {
		JNIEnv * nativeEnvironment;
		void * venv;
	} uenv;

	uenv.venv = nullptr;

	const jint ret = javaVm->GetEnv(&uenv.venv, JNI_VERSION_1_4);

	switch (ret) {
	case JNI_OK:
		return Private{Mode::Existed, Env()};

	case JNI_EDETACHED: {
		const int ret = javaVm->AttachCurrentThread(&uenv.venv, nullptr);
		FLAT_ASSERT(ret == JNI_OK) << ret;
		return Private{Mode::Attached, Env{uenv.nativeEnvironment}};
	}

	default:;
	}

	FLAT_FATAL << ret;
}


Lock::Lock() noexcept :
	p_(_private())
{
}


Lock::~Lock() noexcept
{
	switch (p_.mode) {
	case Mode::Existed:
		break;

	case Mode::Attached: {
		const int ret = javaVm->DetachCurrentThread();
		FLAT_ASSERT(ret == JNI_OK) << ret;
	}
		break;
	}
}




}} // Flat::Jni
