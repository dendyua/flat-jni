
#pragma once

#include <memory>
#include <string>
#include <stdexcept>
#include <string_view>

#include <jni.h>

#include <Flat/Core/SizeOfArray.hpp>




namespace Flat {
namespace Jni {




extern FLAT_JNI_EXPORT void load(JavaVM * vm, jint version);

extern FLAT_JNI_EXPORT jobject localRef(jobject object) noexcept;
extern FLAT_JNI_EXPORT void localUnref(jobject object) noexcept;

extern FLAT_JNI_EXPORT jobject globalRef(jobject object) noexcept;
extern FLAT_JNI_EXPORT void globalUnref(jobject object) noexcept;




class FLAT_JNI_EXPORT Error : public std::runtime_error {
public:
	using std::runtime_error::runtime_error;

	static Error f(char const * format, ...) __attribute__((format (printf, 1, 2)));
};




template <typename T>
class LocalRef {
public:
	static LocalRef<T> adopt(T other) noexcept;

	LocalRef() noexcept;
	~LocalRef() noexcept;

	LocalRef(T other) noexcept;
	LocalRef & operator=(T other) noexcept;

	LocalRef(const LocalRef & other) noexcept;
	LocalRef & operator=(const LocalRef & other) noexcept;

	LocalRef(LocalRef && other) noexcept;
	LocalRef & operator=(LocalRef && other) noexcept;

	operator bool() const noexcept { return object_ != nullptr; }
	T operator*() const noexcept { return object_; }

private:
	struct Adopt {};

private:
	LocalRef(T other, Adopt) noexcept;

private:
	T object_;
};




template <typename T>
class GlobalRef {
public:
	GlobalRef() noexcept;
	~GlobalRef() noexcept;

	GlobalRef(T other) noexcept;
	GlobalRef & operator=(T other) noexcept;

	GlobalRef(const GlobalRef & other) noexcept;
	GlobalRef & operator=(const GlobalRef & other) noexcept;

	GlobalRef(GlobalRef && other) noexcept;
	GlobalRef & operator=(GlobalRef && other) noexcept;

	operator bool() const noexcept { return object_ != nullptr; }
	T operator*() const noexcept { return object_; }

private:
	T object_;
};




typedef LocalRef<jobject>  LocalObject;
typedef LocalRef<jclass>   LocalClass;
typedef LocalRef<jstring>  LocalString;

typedef GlobalRef<jobject> GlobalObject;
typedef GlobalRef<jclass>  GlobalClass;
typedef GlobalRef<jstring> GlobalString;




class FLAT_JNI_EXPORT Env {
public:
	struct Method {
		const char * name;
		const char * signature;
		void * fnPtr;
	};

	struct String {
		const Env & env;
		jstring string;
		std::string_view data;

		String(String && other) noexcept;
		~String() noexcept;
		void operator=(String && other) noexcept;

		String(const String &) = delete;
		void operator=(const String &) = delete;

	private:
		String(const Env & env, jstring string, const std::string_view & data) noexcept :
			env(env), string(string), data(data) {}

		friend class Env;
	};

	Env() noexcept;
	Env(JNIEnv * env) noexcept : env_(env) {}

	JNIEnv * operator->() const noexcept { return env_; }

	jclass getClass(const char * name) const;
	jmethodID getMethod(jclass c, const char * name, const char * signature) const;
	jmethodID getStaticMethod(jclass c, const char * name, const char * signature) const;
	jfieldID getField(jclass c, const char * name, const char * signature) const;
	jfieldID getStaticField(jclass c, const char * name, const char * signature) const;
	void registerNatives(jclass c, const Method * methods, int count) const;

	template <typename T>
	void registerNatives(jclass c, const T & methods) const
	{ this->registerNatives(c, &methods[0], Flat::sizeOfArray(methods)); }

	LocalString newString(const char * data) const;
	String lockString(jstring string) const;
	std::string toStdString(jstring string) const;

private:
	JNIEnv * const env_;
};




class FLAT_JNI_EXPORT Lock {
public:
	Lock() noexcept;
	~Lock() noexcept;

	const Env & env() noexcept;

private:
	enum class Mode {
		Existed,
		Attached
	};

	struct Private {
		const Mode mode;
		const Env & env;
	};

private:
	static Private _private() noexcept;

private:
	const Private p_;
};




template <typename T>
inline LocalRef<T>::LocalRef(const T other, const Adopt) noexcept :
	object_(other)
{}

template <typename T>
inline LocalRef<T> LocalRef<T>::adopt(const T other) noexcept
{ return LocalRef(other, Adopt{}); }

template <typename T>
inline LocalRef<T>::LocalRef() noexcept :
	object_(nullptr)
{}

template <typename T>
inline LocalRef<T>::~LocalRef() noexcept
{ if (object_) localUnref(object_); }

template <typename T>
inline LocalRef<T>::LocalRef(const T other) noexcept :
	object_(other ? static_cast<T>(localRef(other)) : nullptr)
{}

template <typename T>
inline LocalRef<T> & LocalRef<T>::operator=(const T other) noexcept
{
	if (object_) localUnref(object_);
	object_ = other ? static_cast<T>(localRef(other)) : nullptr;
	return *this;
}

template <typename T>
inline LocalRef<T>::LocalRef(const LocalRef<T> & other) noexcept :
	object_(other ? static_cast<T>(localRef(*other)) : nullptr)
{}

template <typename T>
inline LocalRef<T> & LocalRef<T>::operator=(const LocalRef<T> & other) noexcept
{
	if (object_) localUnref(object_);
	object_ = other ? static_cast<T>(localRef(*other)) : nullptr;
	return *this;
}

template <typename T>
LocalRef<T>::LocalRef(LocalRef<T> && other) noexcept :
	object_(other.object_)
{ other.object_ = nullptr; }

template <typename T>
inline LocalRef<T> & LocalRef<T>::operator=(LocalRef<T> && other) noexcept
{ std::swap(object_, other.object_); return *this; }




template <typename T>
inline GlobalRef<T>::GlobalRef() noexcept :
	object_(nullptr)
{}

template <typename T>
inline GlobalRef<T>::~GlobalRef() noexcept
{ if (object_) globalUnref(object_); }

template <typename T>
inline GlobalRef<T>::GlobalRef(const T other) noexcept :
	object_(other ? static_cast<T>(globalRef(other)) : nullptr)
{}

template <typename T>
inline GlobalRef<T> & GlobalRef<T>::operator=(const T other) noexcept
{
	if (object_) globalUnref(object_);
	object_ = other ? static_cast<T>(globalRef(other)) : nullptr;
	return *this;
}

template <typename T>
inline GlobalRef<T>::GlobalRef(const GlobalRef<T> & other) noexcept :
	object_(other ? static_cast<T>(globalRef(*other)) : nullptr)
{}

template <typename T>
inline GlobalRef<T> & GlobalRef<T>::operator=(const GlobalRef<T> & other) noexcept
{
	if (object_) globalUnref(object_);
	object_ = other ? static_cast<T>(globalRef(*other)) : nullptr;
	return *this;
}

template <typename T>
inline GlobalRef<T>::GlobalRef(GlobalRef<T> && other) noexcept :
	object_(other.object_)
{ other.object_ = nullptr; }

template <typename T>
inline GlobalRef<T> & GlobalRef<T>::operator=(GlobalRef<T> && other) noexcept
{ std::swap(object_, other.object_); return *this; }




}} // Flat::Jni
